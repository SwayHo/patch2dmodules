//Modules
const Diagnostics = require('Diagnostics');
const Materials = require('Materials');
const Textures = require('Textures');
const CameraInfo = require('CameraInfo');
const Shaders = require('Shaders');
const R = require('Reactive');

//Assets
const blurMat = Materials.get('rectMat');
const cameraTex = Textures.get('CameraTexture');

const cameraColor = cameraTex.signal;
const texcoords = Shaders.vertexAttribute({ 'variableName': Shaders.VertexAttribute.TEX_COORDS });


// try and test change kernel!!
const kernel = [
    [0, 1, 1, 0], //  2
    [1, 2, 2, 1], //  6
    [1, 2, 2, 1], //  6
    [0, 1, 1, 0] //  2
];

var blurColor = R.pack4(0, 0, 0, 1);
var step = 8;
for (var x = 0; x < 4; x++) {
    for (var y = 0; y < 4; y++) {
        const offsetX = R.div((-1 + x * 1) * step, CameraInfo.previewSize.width);
        const offsetY = R.div((-1 + y * 1) * step, CameraInfo.previewSize.height);
        const movecoords = R.add(texcoords, R.pack2(offsetX, offsetY));
        var sampled = Shaders.textureSampler(cameraColor, movecoords);
        sampled = R.mul(R.div(sampled, 16), kernel[x][y]);
        blurColor = R.add(blurColor, sampled);
    }
}

const contrast = 1.7;

var grayBlur = R.div(R.add(blurColor.x, blurColor.y).add(blurColor.z), 3);

grayBlur = grayBlur.sub(0.5).mul(contrast).add(0.5).sub(0.2);
blurMat.setTexture(R.pack4(grayBlur, grayBlur, grayBlur, 1.0), { textureSlotName: "diffuseTexture" });